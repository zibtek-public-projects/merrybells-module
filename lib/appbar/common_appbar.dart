import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:merrybells_module/res/constants.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
   CustomAppBar({
    Key? key,
    required this.title,
    required this.backgroundColor, this.iconButton,this.iconButton1,this.bottom
  }) : super(key: key);

  final String title;
  final Color backgroundColor;
  IconButton? iconButton;
  IconButton? iconButton1;
  PreferredSizeWidget? bottom;


  @override
  Widget build(BuildContext context) {
    return AppBar(
      bottom: bottom,
      automaticallyImplyLeading: false,
      leading: iconButton1,
      title: Text(title),
      elevation: 0.0,
      actions: [
        iconButton!
      ],
      backgroundColor: backgroundColor,
      flexibleSpace: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25)),
              color: AppColors.merryRed)),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60.0);
}

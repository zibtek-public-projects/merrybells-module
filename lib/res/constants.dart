import 'package:flutter/material.dart';

const APP_NAME = 'Merry_Bells';

class AppText {
  static const welcomeText = 'Welcome to MerryBells';
  static const buyText = 'BUY NOW';
  static const ownText = 'OWNED';
  static const browseText = 'Browse Collections';
  static const purchaseText = 'Browse Purchased Content';
  static const browseButtonText = 'BROWSE';
  static const purchaseButtonText = 'PURCHASED';
  static const linkToOurWeb = 'CHECK OUT OUR WEBSITE!';
  static const mobile = 'MOBILE DATA!';
  static const wifi = 'WIFI!';
  static const notConnected = 'NOT CONNECTED!';
  static const Continue = 'Continue';
  static const termText = 'By continuing you agree to our Terms of Use';
  static const priceList = '\$6.66';
  static const song = 'song1';
  static const album = 'christmas';
  static const viewDetails = 'View Details';
  static const songDetails = 'Song Details';
  static const songName = 'Song Name';
  static const songName1 = 'Christmas Song';
  static const Artist = 'Artist Name';
  static const Artist1 = 'Sassy Sunita';
  static const Albumname = 'Album Name';
  static const Albumname1 = 'Christmas';
  static const genre = 'Genre';
  static const genre1 = 'Melody';
  static const notes = 'Notes';
}

class AppColors {
  static final warningRed = Colors.red[800];
  static final greenAccent700 = Colors.greenAccent[700];
  static final darkGreen = Color(0xff2C6047);
  static final lightGreen = Color(0xffD3F2CD);
  static final merryRed = Color(0xffE02025);
  static final appBlack = Color(0xff141414);
  static final appWhite = Color(0xffFFFFFF);
  static final gray = Color(0xff212121);
  static final gray1 = Color(0xffBFBFBF);
  static final cyan = Color(0Xff00AAFF);
  static final flashColor = Color(0xff2C2C2C);
  static final gray2 = Color(0Xff857F7E);
  static final checkBox = Color(0XffAEAFB1);
  static final pemfMachineBackground = Color(0xff32C5FF);
  static final white = Color(0xffF7F7F7);
}

class PulseText {
  static const login = 'LOG IN';
  static const register = 'REGISTER';
  static const account = 'CREATE ACCOUNT';
  static const fullName = 'Full Name';
  static const phone = 'Phone Number';
  static const email = 'Email Address';
  static const setPassword = 'Set Password';
  static const confirmPassword = 'Confirm Password';
  static const notification = 'I want to receive the Pulse PEMF Notifications';
  static const remember = 'Remember Me';
  static const forgetPassword = 'Forgot Password?';
  static const password = 'Password';
  static const doNotHaveAccount = "Don't have an account?";
  static const signupHere = 'Sign Up Here';
  static const reset = 'Reset Password';
  static const sendOtp = 'SEND OTP';
  static const otp = 'OTP';
  static const continueButton = 'CONTINUE';
  static const nameFiled = 'name';
  static const phoneNumberFiled = 'phoneNumber';
  static const emailFiled = 'email';
  static const passwordFiled = 'password';
  static const confirmPasswordFiled = 'confirmPassword';
  static const passwordNotMatch = 'password not matched';
  static const userAccount = 'USER ACCOUNT';
  static const clear = 'CLEAR';
  static const continuebutton = 'CONTINUE';
  static const accessories = 'PULSE PEMF ACCESSORIES';
  static const machines = 'PEMF MACHINES';
  static const machinesDesc =
      'Please select the Pulse PEMF machines that are currently in use at your location (select all that apply).';
  static const accessoriesDesc =
      'Please select the Pulse PEMF accessories that are currently in use at your location (select all that apply).';
  static const regstrationDesc =
      'Registration Problems or Questions? Contact tech support at 1-888-564-3865 or email us at support@pulsepemf.com. Already have an account? ';
  static const signInHere = 'Sign In Here';
  static const logoutDesc = 'Are you sure you want to logout?';
  static const cancel = 'Cancel';
  static const logout ='Logout';
  static const welcome = 'WELCOME TO PULSE PEMF';
  static const iHaveAcceptedThe = 'I have accepted the ';
  static const termsAndConditions = 'terms and conditions ';
  static const passwordDoesNotMatch = 'password does not match';
  static const pleaseSelectTermsAndCondition = 'Please Select terms and conditions';
  static const accountSetting = 'ACCOUNT';
  static const account_accessories ='Accessories';
  static const enterNewPassword = 'Enter New Password';
  static const confirmNewPassword = 'Confirm New Password';
}

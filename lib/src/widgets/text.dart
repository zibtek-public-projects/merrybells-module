import 'package:flutter/material.dart';

class CommonText extends StatelessWidget {
  final String text;
  final color;
  final double? size;
  final double? letterSpacing;
  final FontWeight fontWeight;
  final String? fontFamily;
  final TextAlign? textAlign;

  const CommonText(
      {required this.text,
      this.color = Colors.black,
      this.size,
      this.letterSpacing,
      this.fontFamily = "",
      this.fontWeight = FontWeight.w400,
      this.textAlign,
      })
      : super();

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
          color: color,
          fontSize: size,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
          fontFamily: fontFamily,
      ),
    );
  }
}

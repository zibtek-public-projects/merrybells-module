import 'package:flutter/material.dart';

class ButtonWidget {
  static button({
    String? title,
    double? width,
    double? height,
    Function? onPress,
    String? fontFamily,
    Color? backgroundColor,
    RoundedRectangleBorder? decorationShape,
    BorderSide? borderSide,
    double? fontSize,
    Color? textColor,
    FontWeight? fontWeight,
    String? nextIcon,
    IconData? icon,
  }) {
    return SizedBox(
        width: width,
        height: height,
        child: ElevatedButton(
          onPressed: () => onPress!(),
          child: Text(
            title!,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: fontSize,
              color: textColor,
              fontWeight: fontWeight,
              fontFamily: fontFamily,
            ),
          ),
          style: ElevatedButton.styleFrom(
            primary: backgroundColor,
            shape: decorationShape,
            side: borderSide,
          ),
        ));
  }

  static buttonStringIcon(
      {String? title,
        double? width,
        double? height,
        Function? onPress,
        String? fontFamily,
        Color? backgroundColor,
        RoundedRectangleBorder? decorationShape,
        BorderSide? borderSide,
        double? fontSize,
        Color? textColor,
        FontWeight? fontWeight,
        String? nextIcon,
        IconData? icon,
        BuildContext? context,
        }) {
    return SizedBox(
        width: width,
        height: height,
        child: ElevatedButton(
          onPressed: () {
            onPress!();
            Navigator.pop(context!);
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 14),
                child: Text(
                  title!,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: fontSize,
                    color: textColor,
                    fontWeight: fontWeight,
                    fontFamily: fontFamily,
                  ),
                ),
              ),
            ],
          ),
          style: ElevatedButton.styleFrom(
            primary: backgroundColor,
            shape: decorationShape,
            side: borderSide,
          ),
        ));
  }
}

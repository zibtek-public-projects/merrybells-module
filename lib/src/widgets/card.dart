import 'package:flutter/material.dart';
import 'package:merrybells_module/common_module.dart';

class CardWidget {
  static commonCard({
    String? title,
    double? width,
    double? height,
    Function? onPress,
    Color? color,
    ImageWidget? ImageWidget,
    RoundedRectangleBorder? decorationShape,
    ClipRRect? clipRRect,
  }) {
    return GestureDetector(
      onTap: () => onPress!(),
      child: Card(
        elevation: 0,
        color: color,
        shape: decorationShape,
        child: SizedBox(width: width, height: height, child: clipRRect),
      ),
    );
  }
}

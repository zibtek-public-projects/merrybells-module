import 'package:flutter/material.dart';

class ScaffoldWidget extends StatelessWidget {
  final Widget? child;
  final Color? color;

  const ScaffoldWidget({Key? key, this.child, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          // Do something here
          print("After clicking the Android Back Button");
          Navigator.pop(context);
          return false;
        },
        child: Scaffold(
          backgroundColor: color,
          body: SingleChildScrollView(
            child: child,
          )
        ),
      ),
    );
  }
}

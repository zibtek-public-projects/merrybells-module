import 'dart:io';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class Utils {
  static RoundedRectangleBorder getRoundedCorners(double val) =>
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(val));

  static void hideKeyboard(BuildContext context) =>
      FocusScope.of(context).requestFocus(FocusNode());

  static String getUrlEndPoint(String url) => url.split('/').last;

  static double getPaddingTop(BuildContext context) {
    if (Platform.isIOS) {
      return MediaQuery.of(context).padding.top;
    } else {
      return MediaQuery.of(context).padding.top + 14;
    }
  }

  static Flushbar showFlushBar({
    required BuildContext context,
    int duration = 1000,
    position = FlushbarPosition.BOTTOM,
    required String content,
    color = Colors.greenAccent,
  }) {
    return Flushbar(
      duration: Duration(seconds: 3),
      margin: EdgeInsets.only(
          left: 10,
          right: 10,
          bottom: 30,
          top: position == FlushbarPosition.TOP ? 20 : 0),
      flushbarPosition: position,
      backgroundColor: color,
      flushbarStyle: FlushbarStyle.FLOATING,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      boxShadows: [
        BoxShadow(
          color: Colors.black54,
          offset: Offset(4, 9),
          blurRadius: 7,
        ),
      ],
      messageText: Align(
        alignment: Alignment.center,
        child: Text(
          content,
          style: TextStyle(
              color: Colors.white,
              fontFamily: 'QSM',
              fontSize: 22,
              letterSpacing: 2),
        ),
      ),
    )..show(context);
  }
}

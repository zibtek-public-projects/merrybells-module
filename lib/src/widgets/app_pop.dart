import 'package:flutter/material.dart';
import '../../common_module.dart';

class PopUpWidget {
  static popUp(
      {String? title,
        Color? titleColor,
        bool? popUpTitle,
        double? titleSize,
        double? popUpHeight,
        double? popUpWidth,
        String? description,
        Color? descColor,
        Color? popUpBgColor,
        double? descSize,
        String? buttonText,
        double? buttonWidth,
        double? buttonHeight,
        Color? backgroundColor,
        RoundedRectangleBorder? decorationShape,
        BorderSide? borderSide,
        double? fontSize,
        double? stringButtonWidth,
        Color? textColor,
        final FontWeight? fontWeight,
        final String? descFamily,
        BuildContext? context,
        Color? backgroundColorText,
        final String? titleFamily,
        final double? descTop,
        final double? titleTop,
        final double? descBottom,
        final double? popUpMarginTop,
        final double? popUpMarginBottom,
        Function()? onPresLog,
        String? buttonTextLogout,
        final bool? logOutButton,
        final String? image,
        final Color? buttonTextColor}) {
    return showDialog(
        context: context!,
        barrierDismissible: false,
        barrierColor:const Color(0xff3c3c43).withOpacity(0.5),
        builder: (context) {
          return Container(
              margin: EdgeInsets.only(
                  top: popUpMarginTop!,
                  left: 0,
                  right: 0,
                  bottom: popUpMarginBottom!),
              child: AlertDialog(
                  backgroundColor: AppColors.appBlack,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  insetPadding: const EdgeInsets.symmetric(horizontal: 18),
                  content: SizedBox(
                      height: popUpHeight,
                      width: popUpWidth,
                      child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: titleTop!),
                              child: CommonText(
                                  size: fontSize,
                                  color: AppColors.appWhite,
                                  text: title!,
                                  fontFamily: titleFamily,
                                 ),
                            ),
                            Padding(
                              padding:
                              EdgeInsets.only(top: descTop!, bottom: descBottom!),
                              child: CommonText(
                                text: description!,
                                color: descColor,
                                size: descSize,
                                fontFamily: descFamily,
                                letterSpacing: 0,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                            logOutButton!
                                ? Container(
                              width: stringButtonWidth,
                              height: 33,
                              margin: const EdgeInsets.only(
                                  top: 15, bottom: 15, left: 0, right: 0),
                              child: ButtonWidget.buttonStringIcon(
                                title: buttonTextLogout,
                                width: buttonWidth,
                                height: buttonHeight,
                                onPress: onPresLog!,
                                context: context,
                                backgroundColor: backgroundColor,
                                decorationShape: decorationShape,
                                borderSide: borderSide,
                                fontSize: fontSize,
                                textColor: textColor,
                                fontWeight: fontWeight,
                                fontFamily: "NimbusBold",
                                nextIcon: "logout",
                              ),
                            )
                                : const SizedBox.shrink(),
                            Container(
                                child: ButtonWidget.button(
                                    title: buttonText,
                                    width: buttonWidth,
                                    height: buttonHeight,
                                    onPress: () {
                                      Navigator.of(context).pop();
                                    },
                                    backgroundColor: backgroundColorText,
                                    fontSize: fontSize,
                                    fontFamily: "NimbusBold",
                                    textColor: buttonTextColor,
                                    fontWeight: fontWeight))]))));
        });
  }
}
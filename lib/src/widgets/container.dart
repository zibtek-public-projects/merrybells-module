import 'package:flutter/material.dart';

class CommonContainer {
  static container({
    double? width,
    double? height,
    Color? color,
    ClipRRect? clipRRect,
  }) {
    return Container(
      color: color,
      child: SizedBox(width: width, height: height, child: clipRRect),
    );
  }
}

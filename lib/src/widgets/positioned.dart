import 'package:flutter/material.dart';

class PositionedImageWidget extends StatelessWidget {
  final String? image;
  final double? height;
  final double? width;
  final double? bottom;
  final double? top;
  final double? left;
  final double? right;
  final BoxFit? fit;

  const PositionedImageWidget(
      {Key? key,
      this.image,
      this.height,
      this.width,
      this.bottom,
      this.top,
      this.left,
      this.right,
      this.fit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: bottom,
      top: top,
      left: left,
      right: right,
      child: Image.asset(
        image!,
        fit: BoxFit.cover,
      ),
    );
  }
}

import 'package:flutter/material.dart';

class ImageWidget extends StatelessWidget {
  final String? url;
  final String? image;
  final double? height;
  final double? width;
  final BoxFit? fit;

  const ImageWidget({Key? key, this.url, this.image, this.height, this.width, this.fit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: width,
        height: height,
        child: Image.asset(
          image!,
          fit: fit,
        ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CommonImageWidget extends StatelessWidget {
  final String? url;
  final double? width;
  final double? height;
  final bool? isCover;
  final String? newFeesPost;

  const CommonImageWidget({
    Key? key,
    @required this.url,
    this.width,
    this.height,
    this.isCover = false,
    this.newFeesPost,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      fit: isCover! ? BoxFit.cover : BoxFit.fitHeight,
      imageUrl: url!,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
      ),
    );
  }
}
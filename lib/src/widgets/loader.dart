import 'package:flutter/material.dart';

loader(enableLoader, context) {
  return enableLoader
      ? showDialog(
          barrierDismissible: false,
          context: context,
          builder: (_) => Center(
            child: Transform.scale(
              scale: 1.0,
              child: CircularProgressIndicator(
                strokeWidth: 5.0,
              ),
            ),
          ),
        )
      : Navigator.of(context).pop();
}

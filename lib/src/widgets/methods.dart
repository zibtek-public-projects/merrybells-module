import 'dart:async';

import 'package:url_launcher/url_launcher.dart';

import '../../common_module.dart';

final Uri _url = Uri.parse('https://merry-bells.com');
Connectivity _connectivity = Connectivity();
late StreamSubscription _streamSubscription;

String _status = "";

String get status => _status;

setStatus(val){
  _status = val;
}


void openBrowseURL() async {
  if (!await launchUrl(_url, webViewConfiguration: WebViewConfiguration()))
    throw 'Could not launch $_url';
}

 checkConnectivity() async {
  var connectionResult = await _connectivity.checkConnectivity();

  if (connectionResult == ConnectivityResult.mobile) {
    _status = "MobileData";
  } else if (connectionResult == ConnectivityResult.wifi) {
    _status = "Wifi";
  } else {
    _status = "Not Connected";
  }
  // setState(() {});
}

 checkRealtimeConnection() {
  _streamSubscription = _connectivity.onConnectivityChanged.listen((event) {
    if (event == ConnectivityResult.mobile) {
      _status = "MobileData";
    } else if (event == ConnectivityResult.wifi) {
      _status = "Wifi";
    } else {
      _status = "Not Connected";
    }
    // setState(() {});
  });
}


